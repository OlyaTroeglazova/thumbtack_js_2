'use strict';

const functions = require('./functions');
const fs = require("fs");

class Order{
    constructor(userId, productList, price) {
        this.id = functions.randomInteger(10000, 99998);
        this.userId = userId; 
        this.date = new Date().toDateString();
        this.productList = productList;
        this.price = price;
    }

    save(user) { // записывает данные о заказе в txt файл
        let info = "ORDER #" + this.id;
        info += "\n\nUSER\t id: " + user.id + '\n\t name: ' + user.name + '\n\t email: ' + user.email + '\n';
        info += "\nDATE\t " + this.date + '\n';
        info += "\nPRODUCTS"
        this.productList.forEach(product => {
            info += " id: " + product.id + "\n\t name: " + product.name + "\n\t price: " + product.price + '\n\n\t';
        });
        fs.writeFileSync("Заказ-" + this.id + ".txt", info);
    }
} 

module.exports.Order = Order;