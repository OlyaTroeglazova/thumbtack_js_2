'use strict';

const functions = require('./functions');
const product = require('./Product');
const order = require('./Order');

class Store {
    
    constructor(countOfProducts) {
        let mapOfProducts = new Map();

        for(let i = 0; i<countOfProducts; i++){
            let name = functions.randomString(4) + ' ' + functions.randomString(6);
            let weight = functions.randomInteger(1,1000);
            let quantityOfProducts = functions.randomInteger(0,1) * functions.randomInteger(10,98);
            let price = functions.randomInteger(100,998);

            let newProduct = new product.Product(name, weight, quantityOfProducts, price);

            mapOfProducts.set(newProduct.id, newProduct);
        }

        this.productList = mapOfProducts;
    }

    getAvailableProducts(){ // возвращает доступные товары
        let availableProducts = new Map();
        this.productList.forEach((product, id) => {
            if(product.quantityOfProducts>0)
                availableProducts.set(id,product);
          });
        return availableProducts;
    }

    displayOfAvailableProducts(){ // выводит в консоль доступные товары
        for (var product of this.productList.values()) {
            if(product.quantityOfProducts>0)
                console.log(product);
          }
    }

    getProductsById(idList){ //по списку айди возвращает map с товарами
        let resultProductList = new Map();
        idList.forEach(id => {
            this.productList.forEach((product,productId) => {
                if(productId==id)
                resultProductList.set(productId, product);
            })
        })
        return resultProductList;
    }
    
    selectProductsToOrder(){ // считывает с консоли айди товаров для покупки, возвращает список айди
        return new Promise((resolve, reject) => {
            process.stdout.write("Please select products to order: ");
            let availableIdList = functions.getIdArrayOfProducts(this.getAvailableProducts());
            var stdin = process.stdin;
            stdin.setRawMode( true );
            stdin.resume();
            stdin.setEncoding( 'utf8' );
    
            let idPproductsToOrder = new Array();
            let id = '';
    
            stdin.on('data', function( key ){

                if ( key === '\u002C' ) { // реагирует на запятую
                    process.stdout.write( key );
                    if (!functions.isExists(availableIdList, id)) {
                        process.stdout.write( '\n' + "This id is not exist. Write another id: " ); 
                        idPproductsToOrder.forEach(key => {
                            process.stdout.write(key + ',');
                        })
                    } else {
                        idPproductsToOrder.push(id);
                    }
                    id='';
                } else if (key == '\u0003'){ // на ctrl+c
                    if(id!=''){
                        if (!functions.isExists(availableIdList, id)) {
                            process.stdout.write( '\n' + "This id is not exist. Write another id: " ); 
                            idPproductsToOrder.forEach(key => {
                                process.stdout.write(key + ',');
                            })
                        } else {
                            idPproductsToOrder.push(id);
                            console.log("\n Thanks for your order! " + idPproductsToOrder);
                            resolve(idPproductsToOrder);
                        }
                    }
                    else {
                        console.log("\n Thanks for your order! " + idPproductsToOrder);
                        resolve(idPproductsToOrder);
                    }

                } else { 
                    process.stdout.write( key );
                    id+=key;
                }
            });
            return idPproductsToOrder;
        });
    }

    toOrder(user, productsToOrder){ // формирование заказа
            let price = 0;
            let orderByUser;
            productsToOrder.forEach(element => {
                price+=element.price;
            })
            if (user.budget < price) {

            } else {
                orderByUser = new order.Order(user.id, productsToOrder, price);
                productsToOrder.forEach(product => {
                    --product.quantityOfProducts;
                })
            }
            return orderByUser;
    }
}

module.exports.Store = Store;