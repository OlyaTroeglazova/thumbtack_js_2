'use strict';

// FILE WITH ADDITIONAL FUNCTIONS

function randomInteger(min, max) { // генерирует рандомное целое число в заданном диапазоне
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  }

function randomString(length) { // генерирует рандомную строку заданной длины
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz";

    for( var i=0; i < length; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function getIdArrayOfProducts(productList) { // возвращает список айди по списку продуктов
    let idArray = new Array();
    let i = -1;
    productList.forEach(element => {
        idArray[++i]=element.id;
    })
    return idArray;
}

function isExists(idList, id) { // содержится ли данный айди в списке
    let res = false;
    idList.forEach(element => {
        if(element==id) {
            res = true;
            return;
        }
    })
    return res;
}

module.exports.randomInteger = randomInteger;
module.exports.randomString = randomString;
module.exports.getIdArrayOfProducts = getIdArrayOfProducts;
module.exports.isExists = isExists;