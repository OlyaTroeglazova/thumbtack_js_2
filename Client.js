'use strict';

const functions = require('./functions');

class Client {
    constructor(name, email, budget) {
        this.id = functions.randomInteger(10000, 99998);
        this.name = name;
        this.email = email;
        this.budget = budget;
    }
}

module.exports.Client = Client;