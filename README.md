1) Создать класс Клиент
С полями
id
имя 
email
бюджет

2) Зарегистрировать пользователя (заполнить поля)
	Имя, бюджет и email вводятся в консоли перед началом программы
	ID генерируется случайным образом (число, 5 разрядов, например 78435)

3) Функция генерации списка товаров
	на вход - количество
	на выход - массив объектов класса Товар
	поля
id (число, 5 разрядов, например 78435)
название (random string, 2 слова 4 и 6 букв)
вес random int
количество товара на складе (random number 2 разряда) 
Цена (random number 3 разряда)

	Примерно трети товаров нет на складе (после генерации)

4) Вывести список доступных товаров в консоль 
Вида id название цена
	Получив список из функции с шага 3 (много товаров не нужно)
	Доступные - значит есть на складе

5) Предложить пользователю выбрать товары для заказа
	Ожидать ввода ID товара через запятую
	Если введен невалидный ID - Вывести сообщение об этом (можно по нажатию  , или Enter), убрать его из строки, которую вводит пользователь, продолжить ввод
	По нажатию Enter принять заказ с введенными id

6) Формирование заказа
	Проверяем, что бюджет пользователя позволяет купить что он там навыбирал
	Иначе сообщение об ошибке (осмысленное) и возврат на шаг 5
	Успешный заказ представляет собой сущность вида 
id заказа
userId
дата создания
товары: [
id товара
]
цена
	После создания заказа ->
	Количество товаров на складе уменьшается

7) Курьеру нужна выписка с данными по заказу и входящим в него товарам
	Вывести в файловую систему рядом с файлом программы (в папку) файл с названием вида Заказ-id заказа.txt
	Содержимое - информация о Пользователе (кроме бюджета), заказе и о входящих в него товарах (кроме количества товара) в читаемом виде (не одной строкой)
	модуль fs
