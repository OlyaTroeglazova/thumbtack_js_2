'use strict';

const client = require('./Client');
const store = require('./Store');

let webStore = new store.Store('3'); 
let user = new client.Client("Olga", "olya@gmail.com", 10000);
webStore.displayOfAvailableProducts();
webStore.selectProductsToOrder().then(IDproductsToOrder => {
    let productsToOrder = webStore.getProductsById(IDproductsToOrder);
    let orderByUser = webStore.toOrder(user, productsToOrder);
    orderByUser.save(user);
    process.exit();
})
.catch();