'use strict';

const functions = require('./functions');

class Product {
    constructor(name, weight, quantityOfProducts, price) {
        this.id = functions.randomInteger(10000, 99998);
        this.name = name;
        this.weight = weight;
        this.quantityOfProducts = quantityOfProducts;
        this.price = price;
    }
}

module.exports.Product = Product;